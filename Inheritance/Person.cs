﻿using System;

namespace Inheritance
{
    public class Person
    {
        protected string Name;
        protected string Surname;
        protected string BirthDate;

        public Person()
        {
            Name = "undefined";
            Surname = "undefined";
            BirthDate = "undefined";
        }

        public Person(string Name, string Surname)
        {
            setName(Name);
            setSurname(Surname);
            BirthDate = "undefined";
        }

        public Person(string Name, string Surname, string BirthDate)
        {
            setName(Name);
            setSurname(Surname);
            setBirthDate(BirthDate);
        }

        public Person(Person obj)
        {
            Name = obj.Name;
            Surname = obj.Surname;
            BirthDate = obj.BirthDate;
        }

        public virtual void ShowInfo()
        {
            Console.WriteLine($"{Name} {Surname} with birth date: {BirthDate}\n");
        }

        public string getName() { return Name; }
        public string getSurname() { return Surname; }
        public string getBirthDate() { return BirthDate; }

        public void setName(string Name) { this.Name = Name; }
        public void setSurname(string Surname) { this.Surname = Surname; }
        public void setBirthDate(string BirthDate) { this.BirthDate = BirthDate; }
    }

    public class Abiturient : LibraryUser
    {
        protected int AverageZNOMark;
        protected int AverageCertificateMark;
        protected string EducationalInstitution;

        public Abiturient()
        {
            AverageZNOMark = 100;
            AverageCertificateMark = 100;
            EducationalInstitution = "undefined";
        }

        public Abiturient(int AverageZNOMark, int AverageCertificateMark, string EducationalInstitution)
        {
            setAverageZNOMark(AverageZNOMark);
            setAverageCertificateMark(AverageCertificateMark);
            setEducationalInstitution(EducationalInstitution);
        }

        public Abiturient(int AverageZNOMark, int AverageCertificateMark, string EducationalInstitution,
            long ReaderTicketNumber, string IssueDate, double MonthlyPayment, string Name, string Surname, string Date)
            :base (ReaderTicketNumber, IssueDate, MonthlyPayment, Name, Surname, Date)
        {
            setAverageZNOMark(AverageZNOMark);
            setAverageCertificateMark(AverageCertificateMark);
            setEducationalInstitution(EducationalInstitution);
        }

        public Abiturient(Abiturient obj)
        {
            AverageZNOMark = obj.AverageZNOMark;
            AverageCertificateMark = obj.AverageCertificateMark;
            EducationalInstitution = obj.EducationalInstitution;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"{Name} {Surname} with birth date: {BirthDate}\nInfo from library:\n" +
                 $"ReaderTicketNumber: {ReaderTicketNumber}; IssueDate: {IssueDate}; MonthlyPayment: {MonthlyPayment}\n" +
                 $"Entered like Abiturient with AverageZNOMark: {AverageZNOMark}, AverageCertificateMark: {AverageCertificateMark}," +
                 $"EducationalInstitution: {EducationalInstitution}\n");
        }

        public int getAverageZNOMark() { return AverageZNOMark; }
        public int getAverageCertificateMark() { return AverageCertificateMark; }
        public string getEducationalInstitution() { return EducationalInstitution; }

        public void setAverageZNOMark(int AverageZNOMark) { this.AverageZNOMark = AverageZNOMark; }
        public void setAverageCertificateMark(int AverageCertificateMark) { this.AverageCertificateMark = AverageCertificateMark; }
        public void setEducationalInstitution(string EducationalInstitution) { this.EducationalInstitution = EducationalInstitution; }
    }

    public class Student : Abiturient
    {
        protected int Stage;
        protected string Group;
        protected string Faculty;
        protected string HighEducationInstitution;

        public Student()
        {
            Stage = 1;
            Group = "undefined";
            Faculty = "undefined";
            HighEducationInstitution = "undefined";
        }

        public Student(int Stage, string Group, string Faculty, string HighEducationInstitution)
        {
            setStage(Stage);
            setGroup(Group);
            setFaculty(Faculty);
            setHighEducationInstitution(HighEducationInstitution);
        }

        public Student(int Stage, string Group, string Faculty, string HighEducationInstitution, int AverageZNOMark, 
            int AverageCertificateMark, string EducationalInstitution, long ReaderTicketNumber, string IssueDate, 
            double MonthlyPayment, string Name, string Surname, string Date)
            :base(AverageZNOMark, AverageCertificateMark, EducationalInstitution, ReaderTicketNumber, IssueDate,
                 MonthlyPayment, Name, Surname, Date)
        {
            setStage(Stage);
            setGroup(Group);
            setFaculty(Faculty);
            setHighEducationInstitution(HighEducationInstitution);
        }

        public Student(Student obj)
        {
            Stage = obj.Stage;
            Group = obj.Group;
            Faculty = obj.Faculty;
            HighEducationInstitution = obj.HighEducationInstitution;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"{Name} {Surname} with birth date: {BirthDate}\nInfo from library:\n" +
                 $"ReaderTicketNumber: {ReaderTicketNumber}; IssueDate: {IssueDate}; MonthlyPayment: {MonthlyPayment}\n" +
                 $"Entered like Abiturient with AverageZNOMark: {AverageZNOMark}, AverageCertificateMark: {AverageCertificateMark}," +
                 $"EducationalInstitution: {EducationalInstitution}\nis studing on {Stage} stage at {Group} group " + 
                 $"on {Faculty} faculty in {HighEducationInstitution}\n");
        }

        public int getStage() { return Stage; }
        public string getGroup() { return Group; }
        public string getFaculty() { return Faculty; }
        public string getHighEducationInstitution() { return HighEducationInstitution; }

        public void setStage(int Stage) { this.Stage = Stage; }
        public void setGroup(string Group) { this.Group = Group; }
        public void setFaculty(string Faculty) { this.Faculty = Faculty; }
        public void setHighEducationInstitution(string HighEducationInstitution) {
            this.HighEducationInstitution = HighEducationInstitution;
        }
    }

    public class Teacher : LibraryUser
    {
        protected string Profession;
        protected string Сhair;
        protected string HighEducationInstitution;

        public Teacher()
        {
            Profession = "undefined";
            Сhair = "undefined";
            HighEducationInstitution = "undefined";
        }

        public Teacher(string Profession, string Сhair, string HighEducationInstitution)
        {
            setProfession(Profession);
            setСhair(Сhair);
            setHighEducationInstitution(HighEducationInstitution);
        }

        public Teacher(string Profession, string Сhair, string HighEducationInstitution, long ReaderTicketNumber,
            string IssueDate, double MonthlyPayment, string Name, string Surname, string Date)
            : base(ReaderTicketNumber, IssueDate, MonthlyPayment, Name, Surname, Date)
        {
            setProfession(Profession);
            setСhair(Сhair);
            setHighEducationInstitution(HighEducationInstitution);
        }

        public Teacher(Teacher obj)
        {
            Profession = obj.Profession;
            Сhair = obj.Сhair;
            HighEducationInstitution = obj.HighEducationInstitution;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"{Name} {Surname} with birth date: {BirthDate}\nInfo from library:\n" +
                 $"ReaderTicketNumber: {ReaderTicketNumber}; IssueDate: {IssueDate}; MonthlyPayment: {MonthlyPayment};\n" +
                 $"Info about teacher:\nProfession: {Profession}; Сhair: {Сhair}; HighEducationInstitution: {HighEducationInstitution};\n");
        }


        public string getProfession() { return Profession; }
        public string getСhair() { return Сhair; }
        public string getHighEducationInstitution() { return HighEducationInstitution; }
        
        public void setProfession(string Profession) { this.Profession = Profession; }
        public void setСhair(string Сhair) { this.Сhair = Сhair; }
        public void setHighEducationInstitution(string HighEducationInstitution)
        {
            this.HighEducationInstitution = HighEducationInstitution;
        }
    }

    public class LibraryUser : Person
    {
        protected long ReaderTicketNumber;
        protected string IssueDate;
        protected double MonthlyPayment;

        public LibraryUser()
        {
            ReaderTicketNumber = 0;
            IssueDate = "undefined";
            MonthlyPayment = 0.0;
        }

        public LibraryUser(long ReaderTicketNumber, string IssueDate, double MonthlyPayment)
        {
            setReaderTicketNumber(ReaderTicketNumber);
            setIssueDate(IssueDate);
            setMonthlyPayment(MonthlyPayment);
        }

        public LibraryUser(long ReaderTicketNumber, string IssueDate, double MonthlyPayment, 
            string Name, string Surname, string Date) : base(Name, Surname, Date)
        {
            setReaderTicketNumber(ReaderTicketNumber);
            setIssueDate(IssueDate);
            setMonthlyPayment(MonthlyPayment);

        }

        public LibraryUser(LibraryUser obj)
        {
            ReaderTicketNumber = obj.ReaderTicketNumber;
            IssueDate = obj.IssueDate;
            MonthlyPayment = obj.MonthlyPayment;
        }

        public override void ShowInfo()
        {
            Console.WriteLine($"{Name} {Surname} with birth date: {BirthDate}\nInfo fromlibrary:\n" +
                 $"ReaderTicketNumber: {ReaderTicketNumber}; IssueDate: {IssueDate}; MonthlyPayment: {MonthlyPayment};\n");
        }

        public long getReaderTicketNumber() { return ReaderTicketNumber; }
        public string getIssueDate() { return IssueDate; }
        public double getMonthlyPayment() { return MonthlyPayment; }

        public void setReaderTicketNumber(long ReaderTicketNumber) { this.ReaderTicketNumber = ReaderTicketNumber; }
        public void setIssueDate(string IssueDate) { this.IssueDate = IssueDate; }
        public void setMonthlyPayment(double MonthlyPayment) { this.MonthlyPayment = MonthlyPayment; }
    }
}
