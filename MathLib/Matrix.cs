﻿using OperatorOverloadingDemo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLib
{

    public class Matrix
    {
        private int Height;
        private int Width;
        Random rnd = new Random();
        Fraction[,] matrix;

        public Matrix(int Width, int Height)
        {
            this.Height = Height;
            this.Width = Width;

            InitMatrix();
        }

        public Matrix(int Width)
        {
            Height = 1;
            this.Width = Width;
            InitMatrix();
        }

        public Matrix()
        {
            Height = 1;
            Width = 1;
            InitMatrix();
        }

        private void InitMatrix()
        {
            matrix = new Fraction[Height, Width];
            for (int i = 0; i < Height; i++)
            {
                for(int g = 0; g < Width; g++)
                {
                    int num1 = rnd.Next(20) + 1;
                    int num2 = rnd.Next(20) + 1;
                    matrix[i, g] = new Fraction(num1, num2);
                    matrix[i, g].ReduceFraction();
                }
            }
        }

        public void Display()
        {
            Console.WriteLine();
            for (int i = 0; i < Height; i++)
            {
                for (int g = 0; g < Width; g++)
                {
                    Console.Write($"{matrix[i, g].ToString()} ");
                }
                Console.WriteLine();
            }
        }

        public static Matrix operator +(Matrix obj)
        {
            return obj;
        }

        public static Matrix operator -(Matrix obj)
        {
            return obj;
        }

        public static Matrix operator +(Matrix obj1, Matrix obj2)
        {
            if (obj1.Width != obj2.Width || obj1.Height != obj2.Height)
            {
                throw new Exception("Widths and Heights Matrices must equals!");
            }
            for (int i = 0; i < obj1.Height; i++)
            {
                for (int g = 0; g < obj1.Width; g++)
                {
                    obj1.matrix[i, g] += obj2.matrix[i, g];
                    obj1.matrix[i, g].ReduceFraction();
                }
               
            }
            return obj1;
        }

        public static Matrix operator -(Matrix obj1, Matrix obj2)
        {
            if (obj1.Width != obj2.Width || obj1.Height != obj2.Height)
            {
                throw new Exception("Widths and Heights Matrices must equals!");
            }
            for (int i = 0; i < obj1.Height; i++)
            {
                for (int g = 0; g < obj1.Width; g++)
                {
                    obj1.matrix[i, g] -= obj2.matrix[i, g];
                    obj1.matrix[i, g].ReduceFraction();
                }

            }
            return obj1;
        }

        public static Matrix operator *(Matrix obj1, Matrix obj2)
        {
            if (obj1.Width != obj2.Height)
            {
                throw new Exception("Width of First Matrix must equals Height of Second Matrix!");
            }
            for (int i = 0; i < obj1.Height; i++)
            {
                for (int g = 0; g < obj1.Width; g++)
                {
                    Fraction res = obj1.matrix[i, 0] * obj2.matrix[0, g];
                    for (int j = 1; j < obj2.Height; j++)
                    {
                        res = res + (obj1.matrix[i, j] * obj2.matrix[j, g]);
                    }
                    res.ReduceFraction();
                    obj1.matrix[i, g] = res;
                }
            }
            return obj1;
        }

        public static Matrix operator /(Matrix obj1, Matrix obj2)
        {
            if (obj1.Width != obj2.Height)
            {
                throw new Exception("Width of First Matrix must equals Height of Second Matrix!");
            }
            for (int i = 0; i < obj1.Height; i++)
            {
                for (int g = 0; g < obj1.Width; g++)
                {
                    Fraction res = obj1.matrix[i, 0] / obj2.matrix[0, g];
                    for (int j = 1; j < obj2.Height; j++)
                    {
                        res = res + (obj1.matrix[i, j] / obj2.matrix[j, g]);
                    }
                    res.ReduceFraction();
                    obj1.matrix[i, g] = res;
                }
            }
            return obj1;
        }

        public void Determinant()
        {
            if(Width != Height)
            {
                throw new Exception("Width must equals Height!");
            }
            Fraction det = new Fraction(0);
            if (Width == 1)
            {
                det = matrix[0, 0];
                det.ReduceFraction();
                Console.WriteLine($"\n{det.ToString()}");
            }
            else if (Width == 2)
            {
                det = matrix[0, 0] * matrix[1, 1] - matrix[1, 0] * matrix[0, 1];
                det.ReduceFraction();
                Console.WriteLine($"\n{det.ToString()}");
            }
            else
            {
                for (int i = 0; i < Width; i++)
                {
                    Fraction currentResult = new Fraction();
                    int heightStatus = 0, widthStatus = i;
                    while (heightStatus != Width)
                    {
                        if (widthStatus >= Width)
                        {
                            widthStatus -= Width;
                        }
                        currentResult = currentResult * matrix[heightStatus, widthStatus];
                        heightStatus++;
                    }
                    det = det + currentResult;
                }

                for (int i = Width - 1; i >= 0; i--)
                {
                    Fraction currentResult = new Fraction(0);
                    int heightStatus = 0, widthStatus = i;
                    while (heightStatus != Width)
                    {
                        if (widthStatus < 0)
                        {
                            widthStatus += Width;
                        }
                        currentResult = currentResult * matrix[heightStatus, widthStatus];
                        heightStatus++;
                    }
                    det = det - currentResult;
                }
                det.ReduceFraction();
                Console.WriteLine($"\n{det.ToString()}");
            }
        }
    }
}
