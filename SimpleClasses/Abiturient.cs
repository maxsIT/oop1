﻿using System;

namespace SimpleClasses
{
    public class Abiturient
    {
        private string Surname = "undefined";
        private string Name = "undefined";
        private string Patronymic = "undefined";
        private string Adress = "undefined";
        private long CertificateNumber = 0;
        private string School = "undefined";
        private int TestResult = 0;

        public Abiturient(string Surname, string Name, string Patronymic)
        {
            SetSurname(Surname);
            SetName(Name);
            SetPatronymic(Patronymic);
        }

        public Abiturient(string Surname, long CertificateNumber, int TestResult)
        {
            SetSurname(Surname);
            SetCertificationNumber(CertificateNumber);
            SetTestResult(TestResult);
        }

        public Abiturient()
        {

        }

        public void SetSurname(string Surname)
        {
            this.Surname = Surname;
        }

        public string GetSurname()
        {
            return Surname;
        }

        public void SetName(string Name)
        {
            this.Name = Name;
        }

        public string GetName()
        {
            return Name;
        }

        public void SetPatronymic(string Patronymic)
        {
            this.Patronymic = Patronymic;
        }

        public string GetPatronymic()
        {
            return Patronymic;
        }

        public void SetAdress(string Adress)
        {
            this.Adress = Adress;
        }

        public string GetAdress()
        {
            return Adress;
        }

        public void SetCertificationNumber(long CertificateNumber)
        {
            if (CertificateNumber > 0) this.CertificateNumber = CertificateNumber;
            else
            {
                this.CertificateNumber = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public long GetCertificateNumber()
        {
            return CertificateNumber;
        }

        public void SetSchool(string School)
        {
            this.School = School;
        }

        public string GetSchool()
        {
            return School;
        }

        public void SetTestResult(int TestResult)
        {
            if (TestResult >= 100 && TestResult <= 200) this.TestResult = TestResult;
            else
            {
                this.TestResult = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetTestResult()
        {
            return TestResult;
        }

        public void Output()
        {
            Console.WriteLine($"Surname = {this.Surname}");
            Console.WriteLine($"Name = {this.Name}");
            Console.WriteLine($"Patronymic = {this.Patronymic}");
            Console.WriteLine($"Adress = {this.Adress}");
            Console.WriteLine($"CertificateNumber = {this.CertificateNumber}");
            Console.WriteLine($"School = {this.School}");
            Console.WriteLine($"TestResult = {this.TestResult}");
        }
    }
}
