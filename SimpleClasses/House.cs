﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleClasses
{
    public class House
    {
        private string Adress = "undefined";
        private byte Floor = 1;
        private int RoomsAmount = 1;
        private double Square = 0;
        private int ConstructionYear = 0;
        private string GPS = "undefined";

        public House(string Adress, int ConstructionYear, string GPS)
        {
            SetAdress(Adress);
            SetConstructionYear(ConstructionYear);
            SetGPS(GPS);
        }

        public House(string Adress, byte Floor, int RoomsAmount,
            double Square, int ConstructionYear, string GPS)
        {
            SetAdress(Adress);
            SetFloor(Floor);
            SetRoomsAmount(RoomsAmount);
            SetSquare(Square);
            SetConstructionYear(ConstructionYear);
            SetGPS(GPS);
        }

        public House()
        {

        }

        public void SetAdress(string Adress)
        {
            this.Adress = Adress;
        }

        public string GetAdress()
        {
            return Adress;
        }

        public void SetFloor(byte Floor)
        {
            if (Floor >= 0) this.Floor = Floor;
            else
            {
                this.Floor = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public byte GetFloor()
        {
            return Floor;
        }

        public void SetRoomsAmount(int RoomsAmount)
        {
            if (RoomsAmount >= 0) this.RoomsAmount = RoomsAmount;
            else
            {
                this.RoomsAmount = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetRoomsAmount()
        {
            return RoomsAmount;
        }

        public void SetSquare(double Square)
        {
            if (Square >= 0) this.Square = Square;
            else
            {
                this.Square = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public double GetSquare()
        {
            return Square;
        }

        public void SetConstructionYear(int ConstructionYear)
        {
            if (ConstructionYear > 0) this.ConstructionYear = ConstructionYear;
            else
            {
                this.ConstructionYear = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetConstructionYear()
        {
            return ConstructionYear;
        }

        public void SetGPS(string GPS)
        {
            this.GPS = GPS;
        }

        public string GetGPS()
        {
            return GPS;
        }

        public void Output()
        {
            Console.WriteLine($"Adress = {this.Adress}");
            Console.WriteLine($"Floor = {this.Floor}");
            Console.WriteLine($"RoomsAmount = {this.RoomsAmount}");
            Console.WriteLine($"Square = {this.Square}");
            Console.WriteLine($"ConstructionYear = {this.ConstructionYear}");
            Console.WriteLine($"GPS = {this.GPS}");
        }
    }
}
