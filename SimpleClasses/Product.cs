﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleClasses
{
    public class Product
    {
        private string Name = "undefined";
        private string Producer = "undefined";
        private double Price = 0;
        private int ExpirationHoldDay = 1;
        private int ExpirationHoldMonth = 1;
        private int ExpirationHoldYear = 2018;
        private int Amount = 0;
        private int ExpirationDay = 1;
        private int ExpirationMonth = 1;
        private int ExpirationYear = 2018;

        public Product(string Name, string Producer, double Price)
        {
            this.Name = Name;
            SetProducer(Producer);
            SetPrice(Price);
        }

        public Product(string Name, string Producer, double Price, int Amount)
        {
            this.Name = Name;
            SetProducer(Producer);
            SetPrice(Price);
            SetAmount(Amount);
        }

        public Product()
        {

        }

        public void SetName(string Name)
        {
            this.Name = Name;
        }

        public string GetName()
        {
            return Name;
        }

        public void SetProducer(string Producer)
        {
            this.Producer = Producer;
        }

        public string GetProducer()
        {
            return Producer;
        }

        public void SetPrice(double Price)
        {
            if (Price >= 0) this.Price = Price;
            else
            {
                this.Price = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public double GetPrice()
        {
            return Price;
        }

        public void SetExpirationHoldDay(int ExpirationHoldDay)
        {
            if (ExpirationHoldDay > 0 && ExpirationHoldDay <= 31) this.ExpirationHoldDay = ExpirationHoldDay;
            else
            {
                this.ExpirationHoldDay = 1;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetExpirationHoldDay()
        {
            return ExpirationHoldDay;
        }

        public void SetExpirationHoldMonth(int ExpirationHoldMonth)
        {
            if (ExpirationHoldMonth > 0 && ExpirationHoldMonth <= 12) this.ExpirationHoldMonth = ExpirationHoldMonth;
            else
            {
                this.ExpirationHoldMonth = 1;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetExpirationHoldMonth()
        {
            return ExpirationHoldMonth;
        }

        public void SetExpirationHoldYear(int ExpirationHoldYear)
        {
            if (ExpirationHoldYear > 2017 && ExpirationHoldMonth <= 2200) this.ExpirationHoldYear = ExpirationHoldYear;
            else
            {
                this.ExpirationHoldYear = 2018;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetExpirationHoldYear()
        {
            return ExpirationHoldYear;
        }

        public void SetAmount(int Amount)
        {
            if (Amount >= 0) this.Amount = Amount;
            else
            {
                this.Amount = 0;
                Console.WriteLine("Incorrect value");
            }
        }

        public float GetAmount()
        {
            return Amount;
        }

        public void SetExpirationDay(int ExpirationDay)
        {
            if (ExpirationHoldDay > 0 && ExpirationDay <= 31) this.ExpirationDay = ExpirationDay;
            else
            {
                this.ExpirationDay = 1;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetExpirationDay()
        {
            return ExpirationDay;
        }

        public void SetExpirationMonth(int ExpirationMonth)
        {
            if (ExpirationMonth > 0 && ExpirationMonth <= 12) this.ExpirationMonth = ExpirationMonth;
            else
            {
                this.ExpirationMonth = 1;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetExpirationMonth()
        {
            return ExpirationMonth;
        }

        public void SetExpirationYear(int ExpirationYear)
        {
            if (ExpirationYear > 2017 && ExpirationYear <= 2200) this.ExpirationYear = ExpirationYear;
            else
            {
                this.ExpirationYear = 2018;
                Console.WriteLine("Incorrect value");
            }
        }

        public int GetExpirationYear()
        {
            return ExpirationYear;
        }

        public int StockAmount { get; set; } = 0;

        public int Rate { get; } = 3;

        public string Review { private get; set; } = "None";

        int ratePosition = 0;

        public int RatePosition
        {
            set
            {
                if (value > 0) ratePosition = value;
                else
                {
                    ratePosition = 0;
                    Console.WriteLine("Incorrect value");
                }
            }

            get
            {
                return ratePosition;
            }
        }

        public void Output()
        {
            Console.WriteLine($"Name = {this.Name}");
            Console.WriteLine($"Producer = {this.Producer}");
            Console.WriteLine($"Price = {this.Price}");
            Console.WriteLine($"ExpirationDate = {this.ExpirationDay}.{this.ExpirationMonth}.{this.ExpirationYear}");
            Console.WriteLine($"Amount = {this.Amount}");
            Console.WriteLine($"ExpirationHoldDate = {this.ExpirationHoldDay}.{this.ExpirationHoldMonth}.{this.ExpirationHoldYear}");
            Console.WriteLine($"StockAmount = {this.StockAmount}");
            Console.WriteLine($"Rate = {this.Rate}");
            Console.WriteLine($"Review = {this.Review}");
            Console.WriteLine($"RatePosition = {this.RatePosition}");
        }
    }
}
