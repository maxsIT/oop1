﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airplane
{
    public class Airplane
    {
        private string StartCity = "undefined";
        private string FinishCity = "undefined";
        private Date StartDate;
        private Date FinishDate;

        public Airplane()
        {

        }

        public Airplane(string StartCity, string FinishCity)
        {
            this.StartCity = StartCity;
            this.FinishCity = FinishCity;
        }

        public Airplane(string StartCity, string FinishCity, Date StartDate, Date FinishDate)
        {
            this.StartCity = StartCity;
            this.FinishCity = FinishCity;
            this.StartDate = new Date(StartDate);
            this.FinishDate = new Date(FinishDate);
        }

        public Airplane(Airplane obj)
        {
            StartCity = obj.StartCity;
            FinishCity = obj.FinishCity;
            StartDate = obj.StartDate;
            FinishDate = obj.FinishDate;
        }

        public void SetStartCity(string StartCity)
        {
            this.StartCity = StartCity;
        }

        public string GetStartCity()
        {
            return StartCity;
        }

        public void SetFinishCity(string FinishCity)
        {
            this.FinishCity = FinishCity;
        }

        public string GetFinishCity()
        {
            return FinishCity;
        }

        public void SetStartDate(Date StartDate)
        {
            this.StartDate = StartDate;
        }

        public Date GetStartDate()
        {
            return StartDate;
        }

        public void SetFinishDate(Date FinishDate)
        {
            this.FinishDate = FinishDate;
        }

        public Date GetFinishDate()
        {
            return FinishDate;
        }

        public bool IsArrivingToday()
        {
            if (StartDate.GetDay() == FinishDate.GetDay() && StartDate.GetMonth() == FinishDate.GetMonth()
                && StartDate.GetYear() == FinishDate.GetYear()) return true;
            else return false;
        }

        public double GetTotalTime()
        {
            DateTime date1 = new DateTime(GetStartDate().GetYear(), GetStartDate().GetMonth(), GetStartDate().GetDay(),
                GetStartDate().GetHours(), GetStartDate().GetMinutes(), 0);
            DateTime date2 = new DateTime(GetFinishDate().GetYear(), GetFinishDate().GetMonth(), GetFinishDate().GetDay(),
                GetFinishDate().GetHours(), GetFinishDate().GetMinutes(), 0);
            return date2.Subtract(date1).TotalMinutes;
        }

    }
}
