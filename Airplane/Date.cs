﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airplane
{
    public class Date
    {
        private int Year = 2018;
        private int Month = 1;
        private int Day = 1;
        private int Hours = 0;
        private int Minutes = 0;

        public Date()
        {

        }

        public Date(int Year, int Month, int Day)
        {
            this.Year = Year;
            this.Month = Month;
            this.Day = Day;
        }

        public Date(int Year, int Month, int Day, int Hours, int Minutes)
        {
            this.Year = Year;
            this.Month = Month;
            this.Day = Day;
            this.Hours = Hours;
            this.Minutes = Minutes;
        }

        public Date(Date obj)
        {
            Year = obj.Year;
            Month = obj.Month;
            Day = obj.Day;
            Hours = obj.Hours;
            Minutes = obj.Minutes;
        }

        public void SetYear(int Year)
        {
            this.Year = Year;
        }

        public int GetYear()
        {
            return Year;
        }

        public void SetMonth(int Month)
        {
            this.Month = Month;
        }

        public int GetMonth()
        {
            return Month;
        }

        public void SetDay(int Day)
        {
            this.Day = Day;
        }

        public int GetDay()
        {
            return Day;
        }

        public void SetHours(int Hours)
        {
            this.Hours = Hours;
        }

        public int GetHours()
        {
            return Hours;
        }

        public void SetMinutes(int Minutes)
        {
            this.Minutes = Minutes;
        }

        public int GetMinutes()
        {
            return Minutes;
        }
    }
}
