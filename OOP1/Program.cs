﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    class Program
    {

        public static Airplane.Airplane[] ReadAirplaneArray(int n)
        {
            Airplane.Airplane[] planes = new Airplane.Airplane[n];
            for (int i = 0; i < n; i++)
            {
                string StartCity = Console.ReadLine();
                string FinishCity = Console.ReadLine();
                int Year_s = Convert.ToInt16(Console.ReadLine());
                int Month_s = Convert.ToInt16(Console.ReadLine());
                int Day_s = Convert.ToInt16(Console.ReadLine());
                int Hours_s = Convert.ToInt16(Console.ReadLine());
                int Minutes_s = Convert.ToInt16(Console.ReadLine());
                Airplane.Date StartDate = new Airplane.Date(Year_s, Month_s, Day_s, Hours_s, Minutes_s);
                int Year_f = Convert.ToInt16(Console.ReadLine());
                int Month_f = Convert.ToInt16(Console.ReadLine());
                int Day_f = Convert.ToInt16(Console.ReadLine());
                int Hours_f = Convert.ToInt16(Console.ReadLine());
                int Minutes_f = Convert.ToInt16(Console.ReadLine());
                Airplane.Date FinishDate = new Airplane.Date(Year_f, Month_f, Day_f, Hours_f, Minutes_f);
                planes[i] = new Airplane.Airplane(StartCity, FinishCity, StartDate, FinishDate);
            }
            return planes;
        }

        public static void PrintAirplane(Airplane.Airplane plane)
        {
            Console.WriteLine($"StartCity = {plane.GetStartCity()}");
            Console.WriteLine($"FinishCity = {plane.GetFinishCity()}");
            string StartDateHours, FinishDateHourse, StartDateMinutes, FinishDateMinutes;
            if (plane.GetStartDate().GetHours() < 10)
            {
                StartDateHours = "0" + Convert.ToString(plane.GetStartDate().GetHours());
            }
            else
            {
                StartDateHours = Convert.ToString(plane.GetStartDate().GetHours());
            }
            if (plane.GetFinishDate().GetHours() < 10)
            {
                FinishDateHourse = "0" + Convert.ToString(plane.GetFinishDate().GetHours());
            }
            else
            {
                FinishDateHourse = Convert.ToString(plane.GetFinishDate().GetHours());
            }
            if (plane.GetStartDate().GetMinutes() < 10)
            {
                StartDateMinutes = "0" + Convert.ToString(plane.GetStartDate().GetMinutes());
            }
            else
            {
                StartDateMinutes = Convert.ToString(plane.GetStartDate().GetMinutes());
            }
            if (plane.GetFinishDate().GetMinutes() < 10)
            {
                FinishDateMinutes = "0" + Convert.ToString(plane.GetFinishDate().GetMinutes());
            }
            else
            {
                FinishDateMinutes = Convert.ToString(plane.GetFinishDate().GetMinutes());
            }
            Console.WriteLine($"StartDate = {plane.GetStartDate().GetDay()}.{plane.GetStartDate().GetMonth()}.{plane.GetStartDate().GetYear()}" +
                $" {StartDateHours}:{StartDateMinutes}");
            Console.WriteLine($"FinishDate = {plane.GetFinishDate().GetDay()}.{plane.GetFinishDate().GetMonth()}.{plane.GetFinishDate().GetYear()}" +
               $" {FinishDateHourse}:{FinishDateMinutes}");
            Console.WriteLine($"IsArrivingToday = {plane.IsArrivingToday()}");
            Console.WriteLine($"GetTotalTime = {plane.GetTotalTime()}");
        }

        public static void PrintAirplanes(Airplane.Airplane[] planes)
        {
            for (int i = 0; i < planes.Length; i++)
            {
                PrintAirplane(planes[i]);
            }
        }

        public static void GetAirplaneInfo(Airplane.Airplane[] planes, out double max_time, out double min_time)
        {
            max_time = double.MinValue;
            min_time = double.MaxValue;
            for(int i = 0; i < planes.Length; i++)
            {
                if(max_time < planes[i].GetTotalTime())
                {
                    max_time = planes[i].GetTotalTime();
                }
                if (min_time > planes[i].GetTotalTime())
                {
                    min_time = planes[i].GetTotalTime();
                }
            }
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }

        public static void SortAirplanesByDate(Airplane.Airplane[] planes)
        {
            bool flag;
            do
            {
                flag = false;
                for (int i = 1; i < planes.Length; i++)
                {
                    DateTime data1 = new DateTime(planes[i].GetStartDate().GetYear(), planes[i].GetStartDate().GetMonth(), planes[i].GetStartDate().GetDay(),
                 planes[i].GetStartDate().GetHours(), planes[i].GetStartDate().GetMinutes(), 0);
                    DateTime data2 = new DateTime(planes[i - 1].GetStartDate().GetYear(), planes[i - 1].GetStartDate().GetMonth(), planes[i - 1].GetStartDate().GetDay(),
                 planes[i - 1].GetStartDate().GetHours(), planes[i - 1].GetStartDate().GetMinutes(), 0);

                    if (data1 < data2)
                    {
                        Swap(ref planes[i], ref planes[i - 1]);
                        flag = true;
                    }
                }
            } while (flag);

            for (int i = planes.Length - 1; i >= 0; i--)
            {
                PrintAirplane(planes[i]);
            }
        }

        public static void SortAirplanesByTotalTime(Airplane.Airplane[] planes)
        {
            bool flag;
            do
            {
                flag = false;
                for (int i = 1; i < planes.Length; i++)
                {

                    if (planes[i].GetTotalTime() < planes[i - 1].GetTotalTime())
                    {
                        Swap(ref planes[i], ref planes[i - 1]);
                        flag = true;
                    }
                }
            } while (flag);

            for (int i = 0; i < planes.Length; i++)
            {
                PrintAirplane(planes[i]);
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Abiturient:");
            SimpleClasses.Abiturient ab1 = new SimpleClasses.Abiturient("Ivanov", "Ivan",
                "Ivanovich");
            SimpleClasses.Abiturient ab2 = new SimpleClasses.Abiturient("Ivanov", 123456,
                200);
            SimpleClasses.Abiturient ab3 = new SimpleClasses.Abiturient();
            Console.WriteLine(ab1.GetSurname());
            ab2.SetSchool("ZSL");
            Console.WriteLine(ab2.GetSchool());
            Console.WriteLine(ab2.GetName());
            ab3.SetTestResult(1999);
            Console.WriteLine(ab3.GetTestResult());
            ab3.SetTestResult(199);
            Console.WriteLine(ab3.GetTestResult());
            Console.WriteLine("\nab1:");
            ab1.Output();
            Console.WriteLine("\nab2:");
            ab2.Output();
            Console.WriteLine("\nab3:");
            ab3.Output();


            Console.WriteLine("\nProduct:");
            SimpleClasses.Product pr1 = new SimpleClasses.Product("Ivanov", "sIT",
                10.5);
            SimpleClasses.Product pr2 = new SimpleClasses.Product("Ivanov", "sIT",
                10.5, 12);
            SimpleClasses.Product pr3 = new SimpleClasses.Product();
            Console.WriteLine(pr1.GetName());
            pr2.SetProducer("ZSL");
            Console.WriteLine(pr2.GetPrice());
            Console.WriteLine(pr2.GetAmount());
            pr3.SetPrice(-10);
            Console.WriteLine(pr3.GetPrice());
            pr3.SetPrice(999.99);
            Console.WriteLine(pr3.GetPrice());
            Console.WriteLine(pr1.StockAmount);
            pr1.StockAmount = 3;
            Console.WriteLine(pr1.StockAmount);
            Console.WriteLine(pr1.Rate);
            pr3.Review = "Nice";
            Console.WriteLine(pr2.RatePosition);
            pr2.RatePosition = 1;
            Console.WriteLine(pr2.RatePosition);

            Console.WriteLine("\npr1:");
            pr1.Output();
            Console.WriteLine("\npr2:");
            pr2.Output();
            Console.WriteLine("\npr3:");
            pr3.Output();


            Console.WriteLine("\nHouse:");
            SimpleClasses.House hs1 = new SimpleClasses.House("IvanStr", 2010,
                "Ivanovka");
            SimpleClasses.House hs2 = new SimpleClasses.House("IvanovStr", 2, 100, 10000.97,
                2010, "Ivanovka");
            SimpleClasses.House hs3 = new SimpleClasses.House();
            Console.WriteLine(hs1.GetAdress());
            hs2.SetSquare(100000.5);
            Console.WriteLine(hs2.GetConstructionYear());
            Console.WriteLine(hs2.GetSquare());
            hs3.SetRoomsAmount(-10);
            Console.WriteLine(hs3.GetRoomsAmount());
            hs3.SetRoomsAmount(4);
            Console.WriteLine(hs3.GetRoomsAmount());

            Console.WriteLine("\nhs1:");
            hs1.Output();
            Console.WriteLine("\nhs2:");
            hs2.Output();
            Console.WriteLine("\nhs3:");
            hs3.Output();

            Console.WriteLine("\n\n\nAirplanes:");
            int n = Convert.ToInt16(Console.ReadLine());
            Airplane.Airplane[] planes = ReadAirplaneArray(n);
            PrintAirplanes(planes);
            double max_time;
            double min_time;
            GetAirplaneInfo(planes, out max_time, out min_time);
            Console.WriteLine($"Minimal time: {min_time}");
            Console.WriteLine($"Maximum time: {max_time}");
            Console.WriteLine("\n\n\n");
            SortAirplanesByDate(planes);

            Console.WriteLine("\n\n\n");
            SortAirplanesByTotalTime(planes);
            Console.ReadKey();
        }
    }
}
