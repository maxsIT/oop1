﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Inheritance.Student student = new Inheritance.Student(1, "PI-59", "FIKT", "ZSTU", 200, 193,
                "ZCL", 100100, "19.12.18", 100.50, "Max", "Sitaylo", "24.08.2000");
            student.ShowInfo();
            Inheritance.Abiturient abiturient = new Inheritance.Abiturient();
            abiturient.ShowInfo();
            abiturient = student;
            abiturient.ShowInfo();
            Inheritance.Person person = new Inheritance.Person("Max", "Sitaylo", "24.08.2000");
            person.ShowInfo();
            Inheritance.Teacher teacher = new Inheritance.Teacher();
            teacher.ShowInfo();
            Console.WriteLine(teacher.getHighEducationInstitution());

            Console.ReadKey();
        }
    }
}
